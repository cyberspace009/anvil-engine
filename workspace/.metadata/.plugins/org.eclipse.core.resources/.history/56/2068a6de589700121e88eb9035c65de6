package edu.buffalo.cse.cse486586.simpledht;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TextView;

public class SimpleDhtProvider extends ContentProvider
{
	//string to contain the node's id
	private String node_id;
	
	//string to contain the textView data
	private TextView _textView;
	
	//the creation of the content provider
	public static Uri CONTENT_URI = Uri.parse("content://edu.buffalo.cse.cse486586.simpledht.provider");
	
	//the string for storing key
	public static final String Key = "key";
		
	//the string for storing values
	public static final String Value = "value";
	
	//String to send the message to the server
    private String _textSend;
	
    //the number of keys
    private static final int TEST_CNT = 50;
    
    //new word will shoe the rows
    private String newWord1;
    private String newWord2;
    
    //content resolver to get retrieve stuff from content provider
    private ContentValues values = new ContentValues();
    
    //content resolver being passed
    private ContentResolver mContentResolver;
    
    //we need a context
    private Context context = null;
    
    //one constructor that takes no argument
	public SimpleDhtProvider()
	{
		//I have nothing to give you. Go away!
	}
	
	//one constructor that takes an argument
	public SimpleDhtProvider(String portStr, TextView tv, ContentResolver _cr)
	{
		mContentResolver = _cr;
		portStr = node_id;
		tv = _textView;
		
		//create the server socket
		ServerSocket serverSock = null;
		try
		{
			serverSock = new ServerSocket(10000);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		//call a asyncTask thread to start the Sender class
		new ServerListener(_textView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverSock);
		
	}
	
	@Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getType(Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
	public Uri insert(Uri uri, ContentValues values)
	{
		//the file key we need to create the internal storage
		String File = values.getAsString("key");
	
		//the file will hold data for values
		String FileV = values.getAsString("value");
		
		//we need to make the internal storage
		try
		{
			//need an output stream to write the data to a file
			FileOutputStream stream = getContext().openFileOutput(File, Context.MODE_PRIVATE);
			
			//a try and catch in case if the file is not found
			try
			{
				//need to write the key data to values
				stream.write(FileV.getBytes());
				
				stream.close();
				
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return uri;
	}

    @Override
    public boolean onCreate()
    {
        // TODO Auto-generated method stub
        return false;
    }

  //query method will allow us to fetch the information from the uri organize key and value.
  	@Override
  	public Cursor query(Uri uri, String[] arg1, String selection, String[] arg3, String arg4)
  	{
  		//we will have the value passed through FileV as we had it written from insert()
  		String FileV = null;
  		
  		//the column for key and value
  		String[] columnStuff = new String[] {Key, Value};
  		
  		//then, we have the matrix cursor to point to the tables of information
  		MatrixCursor matrix = new MatrixCursor(columnStuff);
  		
  		//make a file output stream 
  		FileInputStream stream = null;
  		
  		//a try and catch in case if the file is not found
  		try 
  		{
  			//create the file for the matrix cursor as we did for the insert method
  			stream = getContext().openFileInput(selection);
  			
  			//we need some sort of input to the file on the matrix cursor
  			BufferedReader bReader = new BufferedReader(new InputStreamReader(stream));
  			
  			//another try and catch for only bufferReader and closing the stream
  			try 
  			{
  				//we need to read the line from the bufferReader
  				FileV = bReader.readLine();
  				
  				//we need to close the stream to store the value data inputs
  				stream.close();
  			} catch (IOException e)
  			{
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  			}
  			
  		} catch (FileNotFoundException e)
  		{
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  			
  		//make a column that will pass the key and pass the value through FileV
  		String[] resultRow = {selection, FileV};
  		
  		//add a row for the matrix cursor
  		matrix.addRow(resultRow);
  		
  		return matrix;
  	}

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // TODO Auto-generated method stub
        return 0;
    }

    private String genHash(String input) throws NoSuchAlgorithmException {
        MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        byte[] sha1Hash = sha1.digest(input.getBytes());
        Formatter formatter = new Formatter();
        for (byte b : sha1Hash) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }
//----------------------------------------------------------------------------------------------------------------------------------------------------------
    //the node class
    public class Node 
    {
    	//the number of keys or nodes in the chord ring
    	int keyId;
    	
    	//this will point to other nodes. Thus, successor and predecessor pointers
    	Node next;
    	
    	//initialize the global variables 
    	public Node(String portStr)
    	{
    		node_id = portStr;
    		keyId = 0;
    		next = null;
    	}
    
    	//pass the global variables as a reference
    	public Node(int keyId, Node next)
    	{
    		this.keyId = keyId;
    		this.next = next;
    	}
   
		//finally we add the methods and each method will represent the status of each node on the chord ring
    	//the myKey method will represent the node that will have a successor and a predecessor pointer
    	public String MyKey(String id)
    	{
    		//make the string id hold the node_id
    		id = node_id;
    	
    		//hash the node id
    		try 
    		{
				//hash the node id
    			String nodeHash = genHash(id);
    			
    		} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		return id;
    	}
    
    	//the successor method will represent the node is a successor. It will be pointed to a successor node.
    	public String Succ(String id)
    	{
    		//make the string id hold the node_id
    		id = node_id;
    	
    		//hash the node id
    		try 
    		{
				//hash the node id
    			String nodeHash = genHash(id);
				
    		} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		return id;
    		
    	}
    	
    	//the predecessor method will represent the node is a predecessor. It will be pointed to a predecessor node.
    	public String Pred(String id)
    	{
    		//make the string id hold the node_id
    		id = node_id;
    		
    		//hash the node id
    		try 
    		{
				//hash the node id
    			String nodeHash = genHash(id);

    		} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		return id;
    	}
    	
    	//finally, the method to lookup on the node pointers.
    	public String lookUp(String id)
    	{
    		Node succ = null;
    		Node pred = null;
    		Node my = null;
    		id = node_id;
    		
    		//the id will be a key. The key is parsed as an integer.
    		int keys = Integer.parseInt(id);
    		int Mykey = Integer.parseInt(my.MyKey(id));
    		int Predkey = Integer.parseInt(pred.Pred(id));
    		int Succkey = Integer.parseInt(succ.Succ(id));
    		
    		//the look up method will find the key/id from the chord ring
    		if(keys < Predkey && keys <= Mykey )
			{
    			return my.MyKey(id);
			}else
			{
				return succ.lookUp(id);
			}
    	}
	}
 
//----------------------------------------------------------------------------------------------------------------------------------------------------------
//below here will be the server class and client class
  //the Server Listener class
  	public class ServerListener extends AsyncTask<ServerSocket, String, Void>
  	{
  		//the constructor will receive the text view data from main
  		public ServerListener(TextView textView)
  		{
  			_textView = textView;
  		}
  		
  		@Override
  		protected Void doInBackground(ServerSocket... socket)
  		{
  			try
  			{
  				ServerSocket serverSock = socket[0];

  				//need to pass the message
  				String msg = null;

  				//the while loop will stay on to listen for the client
  				while(true)
  				{
  					//need to create a socket to accept incoming data
  					Socket clientSock = serverSock.accept();

  					//the input stream will be a bufferedReader
  					DataInputStream passIn = new DataInputStream(clientSock.getInputStream());

  					//take in a string to read the message from the line
  					msg = passIn.readUTF();

  					//gotta pass them all, Stringo-mons!
  					publishProgress(msg);

  					//close the socket
  					clientSock.close();
  				}
  			}catch(IOException e)
  			{
  				System.out.println("goto sleep, Ariel. You're tired.");
  				e.printStackTrace();
  			}
  			return null;
  		}
  	
  		//update the ui from the text view
  		protected void onProgressUpdate(String... strings)
		{
			//Steve Ko gave me some good advice about AsyncTask Threads
			//this will return the textview and make some more messages
  			_textView.append(strings[0] + "\n");
            return;
		}
  	}

  	//the Client socket
  	public class ClientSocket extends AsyncTask<String, Void, Void>
  	{
  		@Override
  		protected Void doInBackground(String... params)
  		{
  			//the client's socket
  			Socket clientSock = null;

  			//another client socket
  			Socket clientSock2 = null;

  			//another client socket
  			Socket clientSock3 = null;

  			//need to create the nodes and have them linked.
  			Node Succ = new Node(1, null);
  			Node MyNode = new Node(2, null);
  			Node Pred = new Node(3, null);
  			
  			//finally, point them to each other.
  			Succ.next = null;
  			MyNode.next = Succ;
  			MyNode.next = Pred;
  			Pred.next = null;
  			
  			try
  			{
  				//here will be the if statement for the two avd emulators
  				//Steve Ko provided this idea and command to me
  				if(MyNode.MyKey(node_id).equals("5554"))
  				{
  					//socket for 5554
  					clientSock = new Socket("10.0.2.2", 11112);

  					//socket for 5558
  					node_id = "5558";
  					Succ.Succ("5558");
  					clientSock2 = new Socket("10.0.2.2", 11116);
  					
  					//socket for 5556
  					node_id = "5556";
  					Pred.Succ("5556");
  					clientSock3 = new Socket("10.0.2.2", 11108);
  				
  					//make the message
  					_textSend = newWord1 + newWord2;
  				
  				}else if(MyNode.MyKey(node_id).equals("5556"))
  				{
  					//socket for 5556
  					clientSock = new Socket("10.0.2.2", 11108);

  					//socket for 5558
  					Succ.Succ("5558");
  					clientSock2 = new Socket("10.0.2.2", 11116);

  					//socket for 5554
  					Pred.Pred("5554");
  					clientSock3 = new Socket("10.0.2.2", 11112);
  				}else if(MyNode.MyKey(node_id).equals("5558"))
  				{
  					//socket for 5556
  					clientSock = new Socket("10.0.2.2", 11108);

  					//socket for 5554
  					Pred.Pred("5554");
  					clientSock2 = new Socket("10.0.2.2", 11112);

  					//socket for 5558
  					Succ.Succ("5558");
  					clientSock3 = new Socket("10.0.2.2", 11116);
  				}

  				//make the message
				//_textSend = newWord1 + newWord2;
  				
  				//need a data output stream
  				DataOutputStream passOut = new DataOutputStream(clientSock.getOutputStream());
  				DataOutputStream passOut2 = new DataOutputStream(clientSock2.getOutputStream());
  				DataOutputStream passOut3 = new DataOutputStream(clientSock3.getOutputStream());

  				//send message through the two streams
  				passOut.writeUTF(_textSend);
  				passOut2.writeUTF(_textSend);
  				passOut3.writeUTF(_textSend);

  				//flush the stream and close the sockets
  				passOut.flush();
  				passOut2.flush();
  				passOut3.flush();
  				clientSock.close();
  				clientSock2.close();
  				clientSock3.close();
  			} catch (IOException e)
  			{
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  			}
  			return null;
  		}
  	}

  	//the Ldump class will display all the key value pairs
  	public class LDump extends AsyncTask<Void, String, Void>
  	{

		@Override
		protected Void doInBackground(Void... arg0)
		{
			//store the key value pairs from the current node
			ContentValues[] Cvalues = new ContentValues[TEST_CNT];
			
			Uri newUri;
			
			//make a for loop to store all the key value pairs
			for(int i = 0; i < TEST_CNT; ++i)
			{
				
				//now make another new content value for the array that iterates with i
				Cvalues[i] = new ContentValues();
				
				//put the key and value pairs in the content values 
				Cvalues[i].put(Key, Key + Integer.toString(i));
				Cvalues[i].put(Value, Value + Integer.toString(i));
				
				//finally, store all the key and value pairs in the content provider
				newUri = mContentResolver.insert(CONTENT_URI, Cvalues[i]);
			}
			
			//retrieve all the key and value pairs
			for(int j = 0; j < TEST_CNT; ++j)
			{
				String key = Cvalues[j].getAsString(Key);
				String val = Cvalues[j].getAsString(Value);
				Cursor resultCursor = mContentResolver.query(CONTENT_URI, null, key, null, null);
				String msg = Cvalues[j].getAsString(Key) + " " + Cvalues[j].getAsString(Value); 
				//print the Ldump key and value pairs
				publishProgress(msg);
			}
			
			return null;
		}
  		
		//update the ui from the text view
  		protected void onProgressUpdate(String... strings)
		{
			//Steve Ko gave me some good advice about AsyncTask Threads
			//this will return the textview and make some more messages
  			_textView.append(strings[0] + "\n");
            return;
		}
  	}
}
