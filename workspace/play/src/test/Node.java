package test;

public class Node
{
    int cargo;
    Node next;

    //the location of the nodes
    private String location;
    
    public Node ()
    {
        cargo = 0;
        next = null;
    }

    public Node (int cargo, Node next)
    {
        this.cargo = cargo;
        this.next = next;
    }

    public String home(String place)
    {
    	location = place;
    	
    	return place;
    }
}
