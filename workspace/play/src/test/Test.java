package test;

public class Test 
{
	private Node node;

	private String location;
	
	public Test()
	{
		
	}
	
	public static void main(String[] args)
	{  
		
			//creating the nodes
			Node bob = new Node(1, null);
			Node judy = new Node(2, null);
			Node joe = new Node(3, null);
	
			//linking them to each other
			bob.next = judy;
			judy.next = joe;
			joe.next = bob;
	
			System.out.println("The home of each node: " + bob.home("2389") + " " + judy.home("3455") + " " + joe.home("8345"));
		
	}  
	
	

}
