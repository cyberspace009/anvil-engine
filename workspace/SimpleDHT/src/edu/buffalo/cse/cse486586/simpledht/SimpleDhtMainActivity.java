package edu.buffalo.cse.cse486586.simpledht;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class SimpleDhtMainActivity extends Activity
{
	//string for numbers
	private String portStr;
	
	//string for text view
	private TextView textView;
	
	//create a global variable of the content provider.
	private SimpleDhtProvider provider;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_dht_main);
        
        //need to connect this thing
        TelephonyManager tel = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
  		portStr = tel.getLine1Number().substring(tel.getLine1Number().length() - 4);		
        
  		textView = (TextView) findViewById(R.id.textView1);
  		textView.setMovementMethod(new ScrollingMovementMethod());
        
        //pass data to the provider
        provider = new SimpleDhtProvider(portStr, textView, getContentResolver());
        
        //pass the port string to the node class
    	provider.new Node(portStr);
        
    	//pass the text view data to the server listener class
      	provider.new ServerListener(textView);
        
        //this is the test button
        findViewById(R.id.button3).setOnClickListener(new OnTestClickListener(textView, getContentResolver()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_simple_dht_main, menu);
        return true;
    }

    //the LDump button 
    public boolean LDump(View view)
    {
    	//ldump class will retrieve the key value pairs
    	provider.new LDump().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    	
    	return true;
    }

    //the GDump button

}
