package edu.buffalo.cse.cse486586.simpledynamo;

import java.io.FileNotFoundException;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.widget.TextView;

//the LDump class will display all the key value pairs
public class LDump extends AsyncTask<Void, String, Void>
{
	//the creation of the content provider
//	private static Uri CONTENT_URI = Uri.parse("content://edu.buffalo.cse.cse486586.simpledynamo.provider");
	
	//the string for storing key
  	public static String Key = "key";
  		
  	//the string for storing values
  	public static String Value = "value";
	
	//string to contain the textView data
	private TextView _textView;
	
	//the number of keys
    private static final int TEST_CNT = 20;
    
    //content resolver being passed
    private ContentResolver mContentResolver;
	
    public LDump(ContentResolver _cr, TextView textV)
    {
    	mContentResolver = _cr;
    	_textView = textV;
    }
    
	@Override
	protected Void doInBackground(Void... arg0)
	{
		for(int i = 0; i < TEST_CNT; i++)
		{
			//creating the cursor to check the table
			Cursor resultCursor = mContentResolver.query(SimpleDynamoProvider.CONTENT_URI, null, SimpleDynamoProvider.Key + Integer.toString(i), null, null);
		
			//make the cursor move next
			if(resultCursor.moveToFirst())
			{
				//the name of the file
				String key;
				
				//the value we retrieve from the file "key"
				String value;
		    
				//getting the column index "key" from the content proivder
				int keyColumn = resultCursor.getColumnIndex(SimpleDynamoProvider.Key); 
				
				//getting the column index "value" from the content proivder
				int valueColumn = resultCursor.getColumnIndex(SimpleDynamoProvider.Value);
		
		    	do
		    	{
		    		key = resultCursor.getString(keyColumn);
		    		value = resultCursor.getString(valueColumn);
		    	
		    		//need a string to print the inserted values
					String msg = "<" + key + " >" + "<" + value + ">";
				
					//close the cursor
					resultCursor.close();
				
					//print the message
					publishProgress(msg);
		    
		    	}while(resultCursor.moveToNext());
			}
		}
			return null;
	}	
	
	//update the ui from the text view
	protected void onProgressUpdate(String... strings)
	{
		//Steve Ko gave me some good advice about AsyncTask Threads
		//this will return the textview and make some more messages
		_textView.append(strings[0] + "\n");
        return;
	}
}
