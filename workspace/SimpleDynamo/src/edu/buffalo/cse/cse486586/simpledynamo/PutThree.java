package edu.buffalo.cse.cse486586.simpledynamo;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.TextView;

public class PutThree extends AsyncTask<Void, String, Void>
{
	//the creation of the content provider
	public static Uri CONTENT_URI = Uri.parse("content://edu.buffalo.cse.cse486586.simpledynamo.provider");
	
	//string to contain the textView data
	private TextView _textView;
	
	//the number of keys
    private static final int TEST_CNT = 20;
    
    //content resolver being passed
    private ContentResolver mContentResolver;
	
 	//the string for storing key
  	public static final String Key = "key";
  		
  	//the string for storing values
  	public static final String Value = "value";
	
  	public PutThree(ContentResolver _cr, TextView textV)
  	{
  		mContentResolver = _cr;
  		_textView = textV;
  	}
  	
  	@Override
	protected Void doInBackground(Void... arg0)
	{
  		//create the content values
  		ContentValues[] cv = new ContentValues[TEST_CNT];
		for (int i = 0; i < TEST_CNT; i++) 
		{
			cv[i] = new ContentValues();
			cv[i].put(SimpleDynamoProvider.Key, "key" + Integer.toString(i));
			cv[i].put(SimpleDynamoProvider.Value, "Put3: " + Integer.toString(i));
			
			//thread needs to sleep
			try 
			{
				Thread.sleep(1000);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
			//we need to insert values into the content provider
			mContentResolver.insert(SimpleDynamoProvider.CONTENT_URI, cv[i]);	
		
			String keys = (String) cv[i].get(SimpleDynamoProvider.Key);
			String values = (String) cv[i].get(SimpleDynamoProvider.Value);
			String msg = "<" + keys + " >" + "<" + values + ">";
			
			//print the message
			publishProgress(msg);
		
		}
  			return null;
	}
  
  	//update the ui from the text view
  	protected void onProgressUpdate(String... strings)
  	{
  		//Steve Ko gave me some good advice about AsyncTask Threads
  		//this will return the textview and make some more messages
  		_textView.append(strings[0] + "\n");
          return;
  	}

}
