package edu.buffalo.cse.cse486586.simpledynamo;

public class Node
{
    int cargo;
    Node next;

    //the location of the nodes
    private String node_id;
    
    public Node ()
    {
        cargo = 0;
        next = null;
    }

    public Node (int cargo, Node next)
    {
        this.cargo = cargo;
        this.next = next;
    }

    public String home(String place)
    {
    	//return the hash the value so we can find the location
    	node_id = place;
    	
    	return place;
    }
}