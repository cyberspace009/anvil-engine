#include <stdio.h>

void main()
{
	int a = 50;
	int b = 100;

	void P(int *c, void R(int))
	{
		void Q(int p)
		{
			printf("%d\n", p);
			R(p + 100);
		}

		if(a == b)
		{
			R(b);
		}else
		{
			(*c) = (*c) + 25;
			P(c, Q);
		}
	}

	void Q(int p)
	{
		printf("%d\n", p);
	}

	P(&a, Q);
}
