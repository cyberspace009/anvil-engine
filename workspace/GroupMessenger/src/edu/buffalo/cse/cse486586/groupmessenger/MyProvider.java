package edu.buffalo.cse.cse486586.groupmessenger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.net.Uri;
import android.util.Log;

//the content provider class. Data will be stored here.
public class MyProvider extends ContentProvider
{
	//the creation of the content provider
	public static final Uri CONTENT_URI = Uri.parse("content://edu.buffalo.cse.cse486586.groupmessenger.provider");
	
	//the string for storing key values
	public static final String key = "key";
	
	//the string for storing values
	public static final String value = "value";
	
//We can only use insert and query.
//---------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
	
	//the insert method will allow insertion of data from the content provider
	@Override
	public Uri insert(Uri uri, ContentValues values)
	{
		
		//the file key we need to create the internal storage
		String File = values.getAsString("key");
	
		//the file will hold data for values
		String FileV = values.getAsString("value");
		
		//we need to make the internal storage
		try
		{
			//need an output stream to write the data to a file
			FileOutputStream stream = getContext().openFileOutput(File, Context.MODE_PRIVATE);
			
			//a try and catch in case if the file is not found
			try
			{
				//need to write the key data to values
				stream.write(FileV.getBytes());
				
				stream.close();
				
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return uri;
	}

	//so, we don't use this method.
	@Override
	public boolean onCreate()
	{
		return true;
	}

	//query method will allow us to fetch the information from the uri organize key and value.
	@Override
	public Cursor query(Uri uri, String[] arg1, String selection, String[] arg3, String arg4)
	{
		//we will have the value passed through FileV as we had it written from insert()
		String FileV = null;
		
		//the column for key and value
		String[] columnStuff = new String[] {key, value};
		
		//then, we have the matrix cursor to point to the tables of information
		MatrixCursor matrix = new MatrixCursor(columnStuff);
		
		//make a file output stream 
		FileInputStream stream = null;
		
		//a try and catch in case if the file is not found
		try 
		{
			//create the file for the matrix cursor as we did for the insert method
			stream = getContext().openFileInput(selection);
			
			//we need some sort of input to the file on the matrix cursor
			BufferedReader bReader = new BufferedReader(new InputStreamReader(stream));
			
			//another try and catch for only bufferReader and closing the stream
			try 
			{
				//we need to read the line from the bufferReader
				FileV = bReader.readLine();
				
				//we need to close the stream to store the value data inputs
				stream.close();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		//make a column that will pass the key and pass the value through FileV
		String[] resultRow = {selection, FileV};
		
		//add a row for the matrix cursor
		matrix.addRow(resultRow);
		
		return matrix;
	}
//-----------------------------------------------------------------------------------------------
//we don't want your methods around our town!
	
	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int update(Uri arg0, ContentValues arg1, String arg2, String[] arg3) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}