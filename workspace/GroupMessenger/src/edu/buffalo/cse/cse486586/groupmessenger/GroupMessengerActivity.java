package edu.buffalo.cse.cse486586.groupmessenger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map.Entry;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class GroupMessengerActivity extends Activity
{
	//the text message
	private String _textSend;

	//the textview
	private TextView _textView;

	//the edittext is important
	private EditText _editText;

	//string for numbers
	private String portStr;

	//the sequencer for T-ordering system
	private HashMap<String, Integer> Multi;

	//string to give avd names
	private String avdName = "";

	//the int will be a counter for the sequencer
	private static int i = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		//need to create the MyProvider class
		new MyProvider();

		//initialize hashTable
		Multi = new HashMap<String, Integer>();

		setContentView(R.layout.activity_group_messenger);

		//need to connect this thing
		TelephonyManager tel = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		portStr = tel.getLine1Number().substring(tel.getLine1Number().length() - 4);		

		//create the server socket
		ServerSocket serverSock = null;
		try
		{
			serverSock = new ServerSocket(10000);
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		//set up the edit text an text view
		_textView = (TextView) findViewById(R.id.textView1);
		_textView.setMovementMethod(new ScrollingMovementMethod());
		_editText = (EditText) findViewById(R.id.editText1);

		//call a asyncTask thread to start the Sender class
		new ServerListener().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverSock);

		//this button only starts up the PTest class
		findViewById(R.id.button1).setOnClickListener(
				new OnPTestClickListener(_textView, getContentResolver()));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_group_messenger, menu);
		return true;
	}

	//this button will send the text message
	public boolean buttonSend(View view)
	{
		//Include the editText so that we can edit the text and send it
		_textSend = _editText.getText().toString() + "\n";

		//clear the edit text
		_editText.setText("");

		//call a asyncTask thread to start the Client class
		new ClientSocket().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, portStr, _textSend);
		return true;
	}

	//the test one button. Total Ordering
	public boolean TestOne(View view2)
	{
		//start an AsynchThread for this button 
		new Test1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, portStr, _textSend);

		return true;
	}

	//the test two button. Causul Ordering
	public boolean TestTwo(View view3)
	{

		return true;
	}

	//the Server Listener class
	public class ServerListener extends AsyncTask<ServerSocket, String, Void>
	{
		@Override
		protected Void doInBackground(ServerSocket... socket)
		{
			try
			{
				ServerSocket serverSock = socket[0];

				//need to pass the message
				String msg = null;

				//the while loop will stay on to listen for the client
				while(true)
				{
					//need to create a socket to accept incoming data
					Socket clientSock = serverSock.accept();

					//the input stream will be a bufferedReader
					DataInputStream passIn = new DataInputStream(clientSock.getInputStream());

					//take in a string to read the message from the line
					msg = passIn.readUTF();

					//gotta pass them all, Stringo-mons!
					publishProgress(msg);

					//close the socket
					clientSock.close();
				}
			}catch(IOException e)
			{
				//I was up till 4:00am in the morning working on this
				System.out.println("goto sleep, Ariel. You're tired.");
				e.printStackTrace();
			}
			return null;
		}

		protected void onProgressUpdate(String... strings)
		{
			//Steve Ko gave me some good advice about AsyncTask Threads
			//this will return the textview and make some more messages
			if(Multi.isEmpty() == true)
			{
				_textView = (TextView) findViewById(R.id.textView1);
				//_textView.append(strings[0] + i++ +"\n");
				for( i =0; i <= 4; i++)
				{					
					_textView.append(strings[0] + i + "\n");

				}

				//break this loop by making the hashMap empty
				Multi.clear();
			}


			return;

		}
	}

	//the Client socket
	public class ClientSocket extends AsyncTask<String, Void, Void>
	{
		@Override
		protected Void doInBackground(String... params)
		{
			//the client's socket
			Socket clientSock = null;

			//another client socket
			Socket clientSock2 = null;

			//another client socket
			Socket clientSock3 = null;

			try
			{
				//here will be the if statement for the two avd emulators
				//Steve Ko provided this idea and command to me
				if(portStr.equals("5554"))
				{
					//need to name the avd
					avdName = "avd0: ";

					//socket for 5554
					clientSock = new Socket("10.0.2.2", 11112);

					//socket for 5558
					clientSock2 = new Socket("10.0.2.2", 11116);

					//socket for 5556
					clientSock3 = new Socket("10.0.2.2", 11108);
				}else if(portStr.equals("5556"))
				{
					//need to name the avd
					avdName = "avd1: ";

					//socket for 5556
					clientSock = new Socket("10.0.2.2", 11108);

					//socket for 5558
					clientSock2 = new Socket("10.0.2.2", 11116);

					//socket for 5554
					clientSock3 = new Socket("10.0.2.2", 11112);
				}else if(portStr.equals("5558"))
				{
					//need to name the avd
					avdName = "avd2: ";

					//socket for 5556
					clientSock = new Socket("10.0.2.2", 11108);

					//socket for 5554
					clientSock2 = new Socket("10.0.2.2", 11112);

					//socket for 5558
					clientSock3 = new Socket("10.0.2.2", 11116);
				}

				//need a data output stream
				DataOutputStream passOut = new DataOutputStream(clientSock.getOutputStream());
				DataOutputStream passOut2 = new DataOutputStream(clientSock2.getOutputStream());
				DataOutputStream passOut3 = new DataOutputStream(clientSock3.getOutputStream());

				//set up the message
				_textSend = avdName + _textSend;

				//send message through the two streams
				passOut.writeUTF(_textSend);
				passOut2.writeUTF(_textSend);
				passOut3.writeUTF(_textSend);

				//flush the stream and close the sockets
				passOut.flush();
				passOut2.flush();
				passOut3.flush();
				clientSock.close();
				clientSock2.close();
				clientSock3.close();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
	}

	//the thread for the TestOne
	public class Test1 extends AsyncTask<String, Void, Void>
	{

		@Override
		protected Void doInBackground(String... args)
		{
			//the client's socket
			Socket clientSock = null;

			//another clients socket
			Socket clientSock2 = null;

			//another clients socket
			Socket clientSock3 = null;

			try
			{
				//now we have if statements to detect which avd has sent a message		
				if(portStr.equals("5554"))
				{

					//need to name the avd
					avdName = "avd0";

					//socket for 5554
					clientSock = new Socket("10.0.2.2", 11112);

					//socket for 5558
					clientSock2 = new Socket("10.0.2.2", 11116);

					//socket for 5556
					clientSock3 = new Socket("10.0.2.2", 11108);

				}else if(portStr.equals("5556"))
				{

					//need to name the avd
					avdName = "avd1";

					//socket for 5556
					clientSock = new Socket("10.0.2.2", 11108);

					//socket for 5558
					clientSock2 = new Socket("10.0.2.2", 11116);

					//socket for 5554
					clientSock3 = new Socket("10.0.2.2", 11112);

				}else if(portStr.equals("5558"))
				{

					//need to name the avd
					avdName = "avd2";

					//socket for 5556
					clientSock = new Socket("10.0.2.2", 11108);

					//socket for 5554
					clientSock2 = new Socket("10.0.2.2", 11112);

					//socket for 5558
					clientSock3 = new Socket("10.0.2.2", 11116);
				}

				//need a data output stream to pass the message
				DataOutputStream passOut = new DataOutputStream(clientSock.getOutputStream());

				//then, another data output stream to pass the other avd's message
				DataOutputStream passOut2 = new DataOutputStream(clientSock2.getOutputStream());

				//then, another data output stream to pass the other avd's message
				DataOutputStream passOut3 = new DataOutputStream(clientSock3.getOutputStream());

				//this will take in the avd names and multicast
				_textSend = avdName + ":";

				//the sequencer being stored with items
				Multi.put(_textSend, i);

				//send message through the two streams
				passOut.writeUTF(_textSend);
				passOut2.writeUTF(_textSend);
				passOut3.writeUTF(_textSend);

				//flush the stream and close the sockets
				passOut.flush();
				passOut2.flush();
				passOut3.flush();
				clientSock.close();
				clientSock2.close();
				clientSock3.close();

			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		} 		
	}
}